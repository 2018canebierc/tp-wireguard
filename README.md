# tp-wireguard

`tp-wireguard` est un VPN inspiré de [wireguard](https://www.wireguard.com/), réalisé dans le cadre du cours de programmation système.

Il fonctionne en mode peer-to-peer, avec un nombre arbitraire de pairs reliés à un nœud donné. Toute la configuration se fait dans des fichiers YAML. Des fichiers d'exemple sont donnés dans le dossier `config`.

## Usage

```
tp-wireguard
    Execute the program with default config at example.yml
tp-wireguard keygen
    Generate a private/public key pair
tp-wireguard -c <path>
    Execute the program with custom config file at <path>
tp-wireguard -h
    Show this help
```
