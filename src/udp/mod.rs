pub mod thread;

use std::net::{SocketAddrV4, UdpSocket};

use std::io::Result;

#[derive(Debug)]
pub struct UdpData {
    pub magic: Magic,
    pub payload: Vec<u8>,
    pub addr: SocketAddrV4,
    pub id_dst: Option<u32>,
    pub id_src: Option<u32>,
}

#[derive(Debug)]
#[repr(u8)]
pub enum Magic {
    Hinit = 0,
    Hack = 1,
    Hdata = 2,
}

/// Initialize the udp socket.
pub fn init(port: &u16) -> Result<UdpSocket> {
    UdpSocket::bind(format!("0.0.0.0:{}", port))
}
