use super::{Magic, UdpData};
use log::{error, info, warn};
use std::error::Error;
use std::net::{SocketAddr, UdpSocket};
use std::sync::mpsc::{Receiver, Sender};
use std::sync::Arc;

pub fn u32_to_vec_u8(u: &u32) -> [u8; 4] {
    [
        ((u >> 24) & 255) as u8,
        ((u >> 16) & 255) as u8,
        ((u >> 8) & 255) as u8,
        (u & 255) as u8,
    ]
}

/// Wait for a packet on the udp socket, and dispatch it to the correct peer thread.
///
/// Once a packet is received, we check its magic number, extract headers informations,
/// build the UdpData object and send it to relevant peer threads.
///
/// If it is an ack or a data packet, we know which thread will handle it with the
/// `id_dst` field. Otherwise, we broadcast it.
fn udp_recv(
    socket: &Arc<UdpSocket>,
    recv_send: &Vec<Sender<UdpData>>,
) -> Result<(), Box<dyn Error>> {
    let mut buf: [u8; 65536] = [0; 65536];

    let res = socket.recv_from(&mut buf)?;

    match &res {
        (size, SocketAddr::V4(addr)) => {
            let buf = buf.split_at(*size).0;
            match buf[0] {
                x if x == (Magic::Hinit as u8) => {
                    let id_src = ((buf[1] as u32) << 24)
                        | ((buf[2] as u32) << 16)
                        | ((buf[3] as u32) << 8)
                        | (buf[4] as u32);

                    for sender in recv_send {
                        sender.send(UdpData {
                            magic: Magic::Hinit,
                            id_src: Some(id_src),
                            id_dst: None,
                            addr: *addr,
                            payload: buf.split_at(5).1.to_vec(),
                        })?;
                    }
                }

                x if x == (Magic::Hack as u8) => {
                    let id_dst = ((buf[1] as u32) << 24)
                        | ((buf[2] as u32) << 16)
                        | ((buf[3] as u32) << 8)
                        | (buf[4] as u32);
                    let id_src = ((buf[5] as u32) << 24)
                        | ((buf[6] as u32) << 16)
                        | ((buf[7] as u32) << 8)
                        | (buf[8] as u32);

                    if (id_dst as usize) < recv_send.len() {
                        recv_send[id_dst as usize].send(UdpData {
                            magic: Magic::Hack,
                            id_src: Some(id_src),
                            id_dst: Some(id_dst),
                            addr: *addr,
                            payload: buf.split_at(9).1.to_vec(),
                        })?;
                    }
                }
                x if x == (Magic::Hdata as u8) => {
                    let id_dst = ((buf[1] as u32) << 24)
                        | ((buf[2] as u32) << 16)
                        | ((buf[3] as u32) << 8)
                        | (buf[4] as u32);

                    if (id_dst as usize) < recv_send.len() {
                        recv_send[id_dst as usize].send(UdpData {
                            magic: Magic::Hdata,
                            id_src: None,
                            id_dst: Some(id_dst),
                            addr: *addr,
                            payload: buf.split_at(5).1.to_vec(),
                        })?;
                    }
                }
                _ => warn!("Wrong magic number received"),
            };
        }
        (_, SocketAddr::V6(_)) => (),
    };
    Ok(())
}

/// Thread fontion for the udp receiver thread.
///
/// Loop on the receiver function and handle its errors
pub fn udp_recv_thread(socket: Arc<UdpSocket>, recv_send: Vec<Sender<UdpData>>) {
    loop {
        match udp_recv(&socket, &recv_send) {
            Ok(_) => info!("Udp packet received"),
            Err(e) => error!("Error in udp_recv: {}", e),
        }
    }
}

/// Wait for an UdpData object from peer threads, build the UDP packet and send it.
fn udp_send(socket: &Arc<UdpSocket>, recv_send: &Receiver<UdpData>) -> Result<(), Box<dyn Error>> {
    match recv_send.recv() {
        Ok(res) => {
            let mut msg: Vec<u8>;

            match &res.magic {
                Magic::Hinit => {
                    msg = vec![res.magic as u8];
                    msg.extend_from_slice(&u32_to_vec_u8(
                        &res.id_src.ok_or("Handshake init without source id")?,
                    ));
                }
                Magic::Hack => {
                    msg = vec![res.magic as u8];
                    msg.extend_from_slice(&u32_to_vec_u8(
                        &res.id_dst.ok_or("Handshake ack without destination id")?,
                    ));
                    msg.extend_from_slice(&u32_to_vec_u8(
                        &res.id_src.ok_or("Handshake ack without source id")?,
                    ));
                }
                Magic::Hdata => {
                    msg = vec![res.magic as u8];
                    msg.extend_from_slice(&u32_to_vec_u8(
                        &res.id_dst.ok_or("data without destination id")?,
                    ));
                }
            };

            msg.extend(&res.payload);

            match socket.send_to(&msg, res.addr) {
                _ => (),
            }
        }
        Err(_) => (),
    };

    Ok(())
}

/// Thread fontion for the udp receiver thread.
///
/// Loop on the receiver function and handle its errors
pub fn udp_send_thread(socket: Arc<UdpSocket>, recv_send: Receiver<UdpData>) {
    loop {
        match udp_send(&socket, &recv_send) {
            Ok(_) => info!("Udp packet sent"),
            Err(e) => error!("Error in udp_send: {}", e),
        }
    }
}
