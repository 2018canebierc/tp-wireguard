use crypto_box::{PublicKey, SecretKey};
use std::net::{Ipv4Addr, SocketAddrV4};

pub struct ConnexionInfos {
    pub endpoint: Option<SocketAddrV4>,
    pub pubkey: PublicKey,
    pub privkey: SecretKey,
    pub ip_addr: Option<Ipv4Addr>,
    pub port: u16,

    pub id_src: u32,
    pub id_dst: Option<u32>,
}
