pub mod data;

use super::tun::TunData;
use super::udp::thread::u32_to_vec_u8;
use super::udp::{Magic, UdpData};
use crypto_box::{aead::Aead, Box};
use crypto_box::{PublicKey, SecretKey};
use log::{debug, error, info};
use rand::rngs::OsRng;
use salsa20;
use std::convert::TryInto;
use std::error::Error;
use std::net::SocketAddrV4;
use std::sync::{
    mpsc::{Receiver, Sender},
    Arc, Mutex,
};

/// Generate handshake signature from port and source id used by this peer thread
///
/// We generate a 24 bits nonce, which we use to encrypt the concatenation of
/// `port` and `id_src`, then we return `nonce || ciphertext`
fn sign_gen(
    secret_key: &SecretKey,
    public_key: &PublicKey,
    port: &u16,
    id_src: &u32,
) -> Result<Vec<u8>, std::boxed::Box<dyn Error>> {
    let mut rng = OsRng;
    let nonce = crypto_box::generate_nonce(&mut rng);

    // La payload est les 2 octets du port d'envoi, les 4 octets de l'id
    let mut payload: Vec<u8> = vec![((port >> 8) & 255) as u8, (port & 255) as u8];
    payload.append(&mut Vec::from(u32_to_vec_u8(&id_src)));

    let mut ciphertext = Box::new(&public_key, &secret_key)
        .encrypt(&nonce, &payload[..])
        .or(Err("cannot sign message"))?;

    let mut signature = nonce.clone().to_vec();
    signature.append(&mut ciphertext);
    return Ok(signature);
}

/// Verify a signature with pord and source id of the other peer thread (our destination id)
///
/// We extract the nonce, use it to decrypt the ciphertext, and verify that the encrypted
/// informations are the same than those extracted from udp header and our custom header
fn sign_verif(
    msg: Vec<u8>,
    secret_key: &SecretKey,
    public_key: &PublicKey,
    endpoint: &SocketAddrV4,
    id_dst: &u32,
) -> Result<bool, std::boxed::Box<dyn Error>> {
    let nonce_msg: &[u8] = &msg[0..24];
    let nonce_msg: &[u8; 24] = nonce_msg.try_into()?;

    let nonce = salsa20::XNonce::from_slice(&*nonce_msg);
    let recv_msg = &msg[24..];

    let decrypted_plaintext = Box::new(&public_key, &secret_key)
        .decrypt(&nonce, recv_msg)
        .or(Err("cannot verify signature"))?;

    let endpoint_port_bytes = vec![
        ((endpoint.port() >> 8) & 255) as u8,
        (endpoint.port() & 255) as u8,
    ];

    Ok(decrypted_plaintext.len() == 6
        && decrypted_plaintext[0..2] == endpoint_port_bytes
        && decrypted_plaintext[2..6] == u32_to_vec_u8(&id_dst))
}

/// Generate a signature and return an UDP Data object for init phase of handshake
fn handshake_init_packet(
    secret_key: &SecretKey,
    public_key: &PublicKey,
    endpoint: &SocketAddrV4,
    port: &u16,
    id_src: &u32,
) -> Result<UdpData, std::boxed::Box<dyn Error>> {
    let signature = sign_gen(secret_key, public_key, port, id_src)?;

    Ok(UdpData {
        addr: *endpoint,
        magic: Magic::Hinit,
        payload: signature,
        id_dst: None,
        id_src: Some(*id_src),
    })
}

/// Generate a signature and return an UDP Data object for ack phase of handshake
fn handshake_ack_packet(
    secret_key: &SecretKey,
    public_key: &PublicKey,
    endpoint: &SocketAddrV4,
    port: &u16,
    id_src: &u32,
    id_dst: &u32,
) -> Result<UdpData, std::boxed::Box<dyn Error>> {
    let signature = sign_gen(secret_key, public_key, port, id_src)?;

    Ok(UdpData {
        addr: *endpoint,
        magic: Magic::Hack,
        payload: signature,
        id_dst: Some(*id_dst),
        id_src: Some(*id_src),
    })
}

/// Receive a packet from the tun thread, and transfer it to udp sender thread.
///
/// We verify that we have a destination id (if not, the handshake have not been
/// completed) and an endpoint (if not, we need to wait for the other peer to send
/// us a message). We then encrypt the ip packet, and send it to udp sender thread.
fn sender(
    connexion_infos: &Arc<Mutex<data::ConnexionInfos>>,
    rx: &Receiver<TunData>,
    tx: &Sender<UdpData>,
    rng: &mut OsRng,
    public_key: &crypto_box::PublicKey,
    secret_key: &crypto_box::SecretKey,
    id_src: &u32,
) -> Result<(), std::boxed::Box<dyn Error>> {
    let msg = rx.recv()?;
    info!("Received packet from tun");

    let id_dst = match (*connexion_infos
        .lock()
        .or(Err("poisoned value for connexions_info"))?)
    .id_dst
    .clone()
    {
        None => return Ok(()),
        Some(id_dst) => id_dst,
    };
    debug!("    Destination id is set");

    let endpoint = connexion_infos
        .lock()
        .or(Err("poisoned value for connexions_info"))?
        .endpoint
        .ok_or(format!("no endpoint for {}", id_src))?;

    let nonce = crypto_box::generate_nonce(&mut *rng);
    let mut ciphertext = Box::new(&public_key, &secret_key)
        .encrypt(&nonce, &msg.payload[..])
        .or(Err("cannot encrypt message"))?;
    debug!("    Message encrypted");
    debug!("    Payload: {:?}", ciphertext);
    // On ajoute le nonce au début du message (24 octets)
    let mut send_msg = nonce.clone().to_vec();
    send_msg.append(&mut ciphertext);

    let data = UdpData {
        addr: endpoint,
        magic: Magic::Hdata,
        payload: send_msg,
        id_dst: Some(id_dst),
        id_src: Some(*id_src),
    };

    tx.send(data)?;
    info!("Packet for {} successfuly send to udp", endpoint);

    Ok(())
}

/// Thread fontion for the peer sender thread.
///
/// Send the init packet, then loop, call the sender function
/// and handle its errors
pub fn sender_thread(
    connexion_infos: Arc<Mutex<data::ConnexionInfos>>,
    rx: Receiver<TunData>,
    tx: Sender<UdpData>,
) {
    let mut rng = OsRng;

    let secret_key = (*connexion_infos.lock().unwrap()).privkey.clone();
    let public_key = (*connexion_infos.lock().unwrap()).pubkey.clone();
    let endpoint = (*connexion_infos.lock().unwrap()).endpoint.clone();
    let id_src = (*connexion_infos.lock().unwrap()).id_src.clone();
    let port = (*connexion_infos.lock().unwrap()).port.clone();

    match endpoint {
        Some(e) => match handshake_init_packet(&secret_key, &public_key, &e, &port, &id_src) {
            Ok(packet) => match tx.send(packet) {
                Ok(_) => info!("Send init packet to {}", e),
                Err(e) => error!("Error in sender_thread: {}", e),
            },
            Err(e) => error!("Error in handshake_init_packet: {}", e),
        },
        None => (),
    };

    loop {
        match sender(
            &connexion_infos,
            &rx,
            &tx,
            &mut rng,
            &public_key,
            &secret_key,
            &id_src,
        ) {
            Ok(_) => (),
            Err(e) => error!("Error in peer sender: {}", e),
        }
    }
}

/// Receive a packet from the udp thread, decrypt it, and transfer it to udp sender
/// thread if it's a data packet, or handle it if it's a handshake packet.
///
/// We use the magic number to identify the packet, then :  
///   - if it's an init packet, we check if it is valid, we verify the signature,
/// we update our id_src and endpoint values, then we respond with an ack packet
///   - if it's an ack packet, we update our endpoint and id_src values
///   - if it's a data packet, we decrypt it and send the payload to the tun sender thread
fn receiver(
    connexion_infos: &Arc<Mutex<data::ConnexionInfos>>,
    rx: &Receiver<UdpData>,
    tx: &Sender<TunData>,
    tx_udp: &Sender<UdpData>,
    secret_key: &SecretKey,
    public_key: &PublicKey,
) -> Result<(), std::boxed::Box<dyn Error>> {
    // On reçoit un message du dispatcher coté udp
    let msg = rx.recv()?;
    info!("Received packet from {}", msg.addr);

    match msg.magic {
        Magic::Hinit => {
            info!("    Init packet");
            debug!("    Payload: {:?}", msg.payload);
            let id_dst = match msg.id_src {
                Some(i) => i,
                None => return Err("No source id in handshake init")?,
            };
            if !sign_verif(msg.payload, &secret_key, &public_key, &msg.addr, &id_dst)? {
                Err("Bad handshake init")?;
            }
            let mut mut_c_i = connexion_infos
                .lock()
                .or(Err("poisoned value for connexions_info"))?;
            (*mut_c_i).id_dst = Some(id_dst);
            (*mut_c_i).endpoint = Some(msg.addr);

            let endpoint = mut_c_i.endpoint.clone().ok_or("no endpoint")?;
            let id_src = mut_c_i.id_src.clone();
            let port = mut_c_i.port.clone();

            info!("    Init completed, send ack");
            tx_udp.send(handshake_ack_packet(
                &secret_key,
                &public_key,
                &endpoint,
                &port,
                &id_src,
                &id_dst,
            )?)?;
        }
        Magic::Hack => {
            info!("    Ack packet");
            debug!("    Payload: {:?}", msg.payload);
            let id_dst = match msg.id_dst {
                Some(i) => i,
                None => return Err("No source destination in handshake ack")?,
            };
            if !sign_verif(msg.payload, &secret_key, &public_key, &msg.addr, &id_dst)? {
                return Err("Bad handshake hack")?;
            }
            let mut mut_c_i = connexion_infos
                .lock()
                .or(Err("poisoned value for connexions_info"))?;
            (*mut_c_i).id_dst = msg.id_dst;
            (*mut_c_i).endpoint = Some(msg.addr);
            info!("    Ack completed");
        }
        Magic::Hdata => {
            info!("    Data packet");
            debug!("    Payload: {:?}", msg.payload);
            let nonce_msg: &[u8] = &msg.payload[0..24];
            let nonce_msg: &[u8; 24] = nonce_msg.try_into()?;

            let nonce = salsa20::XNonce::from_slice(&*nonce_msg);
            let recv_msg = &msg.payload[24..];

            let decrypted_plaintext = Box::new(&public_key, &secret_key)
                .decrypt(&nonce, recv_msg)
                .or(Err("cannot decrypt received message"))?;

            info!("    Valid packet, sent to tun");
            tx.send(TunData {
                payload: decrypted_plaintext,
            })?;
        }
    }
    Ok(())
}

/// Thread fontion for the peer receiver thread.
///
/// Loop on the receiver function and handle its errors
pub fn receiver_thread(
    connexion_infos: Arc<Mutex<data::ConnexionInfos>>,
    rx: Receiver<UdpData>,
    tx: Sender<TunData>,
    tx_udp: Sender<UdpData>,
) {
    let secret_key = (*connexion_infos.lock().unwrap()).privkey.clone();
    let public_key = (*connexion_infos.lock().unwrap()).pubkey.clone();

    loop {
        match receiver(
            &connexion_infos,
            &rx,
            &tx,
            &tx_udp,
            &secret_key,
            &public_key,
        ) {
            Ok(_) => (),
            Err(e) => error!("Error in peer receiver: {}", e),
        }
    }
}
