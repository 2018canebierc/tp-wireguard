use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::sync::{mpsc, Arc, Mutex};

use super::tun::TunData;
use crypto_box::{PublicKey, SecretKey};
use hex;
use std::convert::TryInto;
use std::net::{Ipv4Addr, SocketAddrV4};
use yaml_rust::YamlLoader;

pub mod config;
use super::{peer, tun, udp};

use super::udp::UdpData;

/// Generate the config object from a yaml file
fn config_generator(doc: &yaml_rust::Yaml) -> Result<config::Config, Box<dyn Error>> {
    // ip parsing
    let ip_str = doc["host"]["ip"].as_str().ok_or("no host ip")?;
    let ip_vec: Vec<&str> = ip_str.split("/").collect();
    let ip = ip_vec[0].parse::<Ipv4Addr>()?;

    let mask: u8 = ip_vec[1].parse()?;
    let port = doc["host"]["port"].as_i64().ok_or("no host port")? as u16;

    // peer parsing
    let mut peer_vec: Vec<config::PeerConfig> = Vec::new();
    for elt in doc["peers"].as_vec().unwrap_or(&Vec::new()) {
        let ip_str = elt["ip"].as_str().ok_or("no peer ip")?;
        let ip_peer = ip_str.parse::<Ipv4Addr>()?;

        let endpoint: Option<SocketAddrV4> = match elt["endpoint"].as_str() {
            Some(s) => Some(s.parse::<SocketAddrV4>()?),
            _ => None,
        };

        let pubkey: [u8; 32] = hex::decode(elt["pubkey"].as_str().ok_or("no peer pubkey")?)?
            .try_into()
            .unwrap();

        peer_vec.push(config::PeerConfig {
            pubkey: PublicKey::from(pubkey),
            ip: ip_peer,
            endpoint,
        })
    }

    let privkey: [u8; 32] = hex::decode(doc["host"]["privkey"].as_str().ok_or("no privkey")?)?
        .try_into()
        .unwrap();

    let config = config::Config {
        privkey: SecretKey::from(privkey),
        ip,
        port,
        mask,
        peers: peer_vec,
    };

    Ok(config)
}

/// Generate a config object from a config file path
pub fn parse_config(path: &Path) -> Result<config::Config, Box<dyn Error>> {
    let mut file = File::open(path)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let docs = YamlLoader::load_from_str(&contents)?;

    Ok(config_generator(&docs[0])?)
}

/// Init all threads, then wait the last one
pub fn thread_init(config: Arc<config::Config>) {
    // From tun to peer thread (for all the senders)
    let mut tun_send: Vec<mpsc::Sender<TunData>> = Vec::new();
    // From udp to peer thread (for all the senders)
    let mut udp_send: Vec<mpsc::Sender<UdpData>> = Vec::new();

    // From all peer threads to udp
    let (send_to_udp, recv_to_udp) = mpsc::channel::<UdpData>();
    // From all peer threads to tun
    let (send_to_tun, recv_to_tun) = mpsc::channel::<TunData>();

    // For each peer, we :
    //   - create the two channels from udp and tun to this peer
    //   - create the shared memory for the two peer threads
    //   - create a copy of the senders to tun and udp threads for the peer threads
    //   - create a second copy of the sender to udp for the receiver thread (for handshake)
    //   - create the threads
    //   - add the senders of the two channels from the beggining to tun and udp threads
    for (i, elt) in (&config.peers).iter().enumerate() {
        // From udp to peer thread
        let (send_from_udp, recv_from_udp) = mpsc::channel::<UdpData>();
        // From tun to peer thread
        let (send_from_tun, recv_from_tun) = mpsc::channel::<TunData>();

        let connexion_infos = Arc::new(Mutex::new(peer::data::ConnexionInfos {
            endpoint: elt.endpoint,
            pubkey: elt.pubkey.clone(),
            privkey: config.privkey.clone(),
            ip_addr: Some(elt.ip),
            port: config.port,
            id_src: i.try_into().unwrap(),
            id_dst: None,
        }));
        let connexion_infos_copy = Arc::clone(&connexion_infos);

        let s_tun = send_to_tun.clone();
        // We need this pipe to send handshake packets
        let s_udp_from_tun = send_to_udp.clone();
        let s_udp = send_to_udp.clone();

        std::thread::spawn(move || {
            peer::receiver_thread(connexion_infos, recv_from_udp, s_tun, s_udp_from_tun);
        });
        std::thread::spawn(move || {
            peer::sender_thread(connexion_infos_copy, recv_from_tun, s_udp);
        });

        tun_send.push(send_from_tun);
        udp_send.push(send_from_udp);
    }

    // Finally, we can create udp and tun threads
    // We wait for the last one because all threads terminate only when
    // we close the app, so we can wait for any thread

    let iface = Arc::new(tun::init(&config.ip, &config.mask).unwrap());
    let iface_copy = Arc::clone(&iface);
    let udp_socket = Arc::new(udp::init(&config.port).unwrap());
    let udp_socket_copy = Arc::clone(&udp_socket);

    // TUN threads
    std::thread::spawn(move || {
        tun::thread::tun_recv_thread(Arc::clone(&config), iface, tun_send);
    });
    std::thread::spawn(move || {
        tun::thread::tun_send_thread(iface_copy, recv_to_tun);
    });

    // UDP Threads
    std::thread::spawn(move || {
        udp::thread::udp_recv_thread(udp_socket, udp_send);
    });
    std::thread::spawn(move || {
        udp::thread::udp_send_thread(udp_socket_copy, recv_to_udp);
    })
    .join()
    .unwrap();
}
