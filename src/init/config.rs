use crypto_box::{PublicKey, SecretKey};
use std::net::{Ipv4Addr, SocketAddrV4};

#[derive(Debug)]
pub struct PeerConfig {
    pub pubkey: PublicKey,
    pub endpoint: Option<SocketAddrV4>,
    pub ip: Ipv4Addr,
}

#[derive(Debug)]
pub struct Config {
    pub privkey: SecretKey,
    pub ip: Ipv4Addr,
    pub port: u16,
    pub mask: u8,

    pub peers: Vec<PeerConfig>,
}
