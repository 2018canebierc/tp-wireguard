pub mod init;
pub mod peer;
pub mod tun;
pub mod udp;

use crypto_box::SecretKey;
use hex;
use log::error;
use rand::rngs::OsRng;
use std::env;
use std::path::Path;
use std::sync::Arc;

fn print_help() {
    println!("Usage: ");
    println!("  wireguard");
    println!("      Execute the program with default config at example.yml");
    println!("  wireguard keygen");
    println!("      Generate a private/public key pair");
    println!("  wireguard -c <path>");
    println!("      Execute the program with custom config file at <path>");
    println!("  wireguard -h");
    println!("      Show this help");
}

fn keygen() {
    let mut rng = OsRng;
    let secret_key = SecretKey::generate(&mut rng);
    let public_key = secret_key.public_key();

    println!("privkey: {}", hex::encode(secret_key.to_bytes()));
    println!("pubkey:  {}", hex::encode(public_key.as_bytes()));
}

fn main() {
    env_logger::init();

    let args: Vec<String> = env::args().collect();
    let mut path = "example.yml";
    if args.len() != 1 {
        if args[1] == "keygen" {
            keygen();
            return;
        } else if args.len() >= 3 && args[1] == "-c" {
            path = &args[2];
        } else {
            print_help();
            return;
        }
    }

    match init::parse_config(Path::new(path)) {
        Ok(c) => {
            init::thread_init(Arc::new(c));
        }
        Err(e) => {
            error!("Config file failed");
            error!("Error: {:?}", e);
            panic!("Could not read config file");
        }
    }
}
