use std::sync::mpsc::{Receiver, Sender};

use super::super::init::config;
use super::TunData;
use log::{error, info, warn};
use std::collections::HashMap;
use std::error::Error;
use std::net::Ipv4Addr;
use std::sync::Arc;
use tun_tap::Iface;

/// Wait for a packet on the tun interface, and dispatch it to the correct peer thread.
///
/// The peer thread is determinated with a HashMap which associate the peer ip with
/// the id of the associated peer thread.
fn tun_recv(
    iface: &Arc<Iface>,
    sender_vec: &Vec<Sender<TunData>>,
    ip_to_thread: &HashMap<Ipv4Addr, u32>,
) -> Result<(), Box<dyn Error>> {
    let mut buf = vec![0; super::TUN_PACKET_SIZE];
    let long = iface.recv(&mut buf)?;
    buf = buf.split_at(long).0.to_vec();

    // min ipv4 header size
    if long > 20 {
        let ip_dest = Ipv4Addr::new(buf[20], buf[21], buf[22], buf[23]);
        let thread_id: usize = match ip_to_thread.get(&ip_dest) {
            Some(id) => *id as usize,
            None => {
                warn!("no thread id was assigned to {}", &ip_dest);
                return Ok(());
            }
        };
        sender_vec[thread_id].send(TunData { payload: buf })?;
    } else {
        warn!("invalid ipv4 header");
    }

    Ok(())
}

/// Thread fontion for the tun receiver thread.
///
/// Initialize the HashMap for ip/peer thread association, then
/// loop on the receiver function and handle its errors
pub fn tun_recv_thread(
    config: Arc<config::Config>,
    iface: Arc<Iface>,
    sender_vec: Vec<Sender<TunData>>,
) {
    let mut ip_to_thread: HashMap<Ipv4Addr, u32> = HashMap::new();
    for (i, elt) in config.peers.iter().enumerate() {
        ip_to_thread.insert(elt.ip, i as u32);
    }

    loop {
        match tun_recv(&iface, &sender_vec, &ip_to_thread) {
            Ok(_) => info!("Tun packet received"),
            Err(e) => error!("Error in tun_recv: {}", e),
        }
    }
}

/// Wait for a packet from peer threads, and forward it to the tun interface
fn tun_send(iface: &Arc<Iface>, recv: &Receiver<TunData>) -> Result<(), Box<dyn Error>> {
    let payload = recv.recv()?.payload;
    iface.send(&payload).unwrap_or(0);
    Ok(())
}

/// Thread fontion for the tun sender thread.
///
/// Loop on the sender function and handle its errors
pub fn tun_send_thread(iface: Arc<Iface>, recv: Receiver<TunData>) {
    loop {
        match tun_send(&iface, &recv) {
            Ok(_) => info!("Tun packet sent"),
            Err(e) => error!("Error in tun_send: {}", e),
        }
    }
}
