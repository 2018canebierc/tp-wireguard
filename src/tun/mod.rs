use tun_tap::Iface;

use std::error::Error;

use std::process::Command;

use std::net::Ipv4Addr;
pub mod thread;

/// Initialize the tun interface.
///
/// Create the tun interface, then set its ip address and up it
pub fn init(ip: &Ipv4Addr, mask: &u8) -> Result<Iface, Box<dyn Error>> {
    let iface = Iface::new("tun%d", tun_tap::Mode::Tun)?;

    Command::new("ip")
        .args(&[
            "addr",
            "add",
            "dev",
            iface.name(),
            &format!("{}/{}", ip, mask),
        ])
        .output()?;
    Command::new("ip")
        .args(&["link", "set", "up", "dev", iface.name()])
        .output()?;

    Ok(iface)
}

pub struct TunData {
    pub payload: Vec<u8>,
}

pub const TUN_PACKET_SIZE: usize = 1504;
